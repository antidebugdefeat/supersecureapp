# SuperSecureApp #

A test Android application to try with the [AntiDebugDefeat tool](https://bitbucket.org/antidebugdefeat/antidebugdefeat).

# Usage #
1. Clone the repository
2. Open in Android Studio
3. Build the app and install with ADB

**Alternatively**, you can download the compiled release [supersecureapp-release.apk](https://bitbucket.org/antidebugdefeat/supersecureapp/downloads/supersecureapp-release.apk).
