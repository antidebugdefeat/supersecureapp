package com.example.supersecureapp;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;


public class ServerCommunication {


    public static String sendLoginData(String id, String password){
        try {
            Context ctx = LoginActivity.getContext();
            AssetManager am = ctx.getAssets();

            // Certificate pinning
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            InputStream caInput = new BufferedInputStream(am.open("le_cert.crt"));
            Certificate ca;
            ca = cf.generateCertificate(caInput);
            System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());

            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, tmf.getTrustManagers(), null);

            // Prepare query
            String query = "user="+ URLEncoder.encode(id,"UTF-8");
            query += "&";
            query += "password="+URLEncoder.encode(password,"UTF-8") ;
            // Send the request
            URL url = new URL( "https://httpbin.org/post");
            HttpsURLConnection urlConnection = (HttpsURLConnection)url.openConnection();
            urlConnection.setSSLSocketFactory(context.getSocketFactory()); // Use our TrustManager
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
            urlConnection.setRequestProperty("Content-length", String.valueOf(query.length()));
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);

            DataOutputStream output = new DataOutputStream(urlConnection.getOutputStream());
            output.writeBytes(query);
            output.close();
            DataInputStream input = new DataInputStream(urlConnection.getInputStream());
            StringBuilder response = new StringBuilder();

            // Process the response
            for( int c = input.read(); c != -1; c = input.read() )
                response.append((char)c);
            response.append("\n");
            input.close();

            return response.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "Connection error";

    }
}
