package com.example.supersecureapp;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import static com.example.supersecureapp.Security.isPhoneRooted;
import static com.example.supersecureapp.ServerCommunication.sendLoginData;


public class LoginActivity extends AppCompatActivity {

    private static Context mContext;
    private UserLoginTask mAuthTask = null;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mPhoneRooted;
    private View mLoginSuccess;
    private View mResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_login);
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mPhoneRooted = findViewById(R.id.phone_rooted);
        mLoginSuccess = findViewById(R.id.login_success);
        mResponse = findViewById(R.id.response);
    }


    private void attemptLogin() {
        mPhoneRooted.setVisibility(View.GONE);
        mLoginSuccess.setVisibility(View.GONE);
        mResponse.setVisibility(View.GONE);

        if (isPhoneRooted()){
            mPhoneRooted.setVisibility(View.VISIBLE);
            return;
        }
        if (mAuthTask != null) {
            return;
        }

        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        new UserLoginTask().execute(email, password);

    }

    public class UserLoginTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            return sendLoginData(params[0], params[1]);
        }

        @Override
        protected void onPostExecute(final String result) {
            mAuthTask = null;
            TextView mResponseTextView = (TextView)mResponse;
            mResponseTextView.setText(result);
            mLoginSuccess.setVisibility(View.VISIBLE);
            mResponse.setVisibility(View.VISIBLE);


        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
        }
    }

    public static Context getContext(){
        return mContext;
    }
}